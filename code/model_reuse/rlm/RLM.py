"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 22/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

import pandas as pd


class RLM:
    """
    Base class that represents a Regression Linear Model
    """
    def __init__(self, file_path: str):
        self.rlm_name = file_path.split('/')[-1].split('.')[0]
        self.data = pd.read_csv(file_path)
        self.mae_on_deploy = float('inf')
        print(self.rlm_name)

    def get_mae_on_deploy(self):
        return self.mae_on_deploy

    def set_mae_on_deploy(self, mae: float):
        self.mae_on_deploy = mae

    def predict(self, test_data):
        """
        :param test_data: one o more instances that will be predicted
        :return: prediction or predictions
        """
        pass



