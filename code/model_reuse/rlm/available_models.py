"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 24/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

from enum import Enum


class BaselineModels(Enum):
    BASE_AGO = 'base_ago'
    BASE_AGO_FPROFDIFF = 'base_ago_fprofdiff'
    BASE_AGO_FPROFDIFF_FPROF = 'base_ago_fprofdiff_fprof'
    BASE_AGO_SPROFDIFF = 'base_ago_sprofdiff'
    BASE_AGO_SPROFDIFF_SPROF = 'base_ago_sprofdiff_sprof'
    BASE_FPROF = 'base_fprof'
    BASE_SPROF = 'base_sprof'


class StationModels(Enum):
    RLM_FULL = 'rlm_full'
    RLM_FULL_TEMP = 'rlm_full_temp'
    RLM_SHORT = 'rlm_short'
    RLM_SHORT_FULL = 'rlm_short_full'
    RLM_SHORT_FULL_TEMP = 'rlm_short_full_temp'
    RLM_SHORT_TEMP = 'rlm_short_temp'


def get_baseline_models():
    return [model.name for model in BaselineModels]


def get_station_models():
    return [model.name for model in StationModels]
