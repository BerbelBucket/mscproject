"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 23/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

from .rlm import RLM
from model_reuse.rlm.available_models import StationModels as sM


class StationRLM(RLM):
    def __init__(self, model, station_id):
        if not isinstance(model, sM):
            raise ValueError('{} is not a valid Station Regression Linear Model'.format(model))
        super().__init__(model.value, station_id)


class RlmFull(StationRLM):
    def __init__(self, station_id):
        super().__init__(sM.RLM_FULL, station_id)


class RlmFullTemp(StationRLM):
    def __init__(self, station_id):
        super().__init__(sM.RLM_FULL_TEMP, station_id)


class RlmShort(StationRLM):
    def __init__(self, station_id):
        super().__init__(sM.RLM_SHORT, station_id)


class RlmShortFull(StationRLM):
    def __init__(self, station_id):
        super().__init__(sM.RLM_SHORT_FULL, station_id)


class RlmShortFullTemp(StationRLM):
    def __init__(self, station_id):
        super().__init__(sM.RLM_SHORT_FULL_TEMP, station_id)


class RlmShortTemp(StationRLM):
    def __init__(self, station_id):
        super().__init__(sM.RLM_SHORT_TEMP, station_id)
