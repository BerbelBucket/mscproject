"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 22/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

from model_reuse.utils.rlm_ops import get_features_from_csv
from utils.features import Features
from abc import ABC


class RLM(ABC):
    """
    Base class that represents a Regression Linear Model
    """
    EXTENSION = '.csv'

    def __init__(self, model_name, station_id=-1):
        self.rlm_name: str = model_name
        self.intercept, self.features = get_features_from_csv(self._format_model_name(model_name, station_id))
        self.mae_on_deploy: float = float('inf')

    def _format_model_name(self, model_name, station_id):
        baseline_name = 'model_{}'.format(model_name) + self.EXTENSION
        station_name = 'model_station_{}_{}'.format(station_id, model_name) + self.EXTENSION
        return baseline_name if station_id == -1 else station_name

    def get_mae_on_deploy(self):
        return self.mae_on_deploy

    def set_mae_on_deploy(self, mae: float):
        self.mae_on_deploy = mae

    def predict(self, test_data):
        """
        :param test_data: instance that will be predicted
        :return: prediction
        """
        return round(self.intercept + sum(
            test_data[Features.get_index(feature)] * weight for feature, weight in self.features.items()))
