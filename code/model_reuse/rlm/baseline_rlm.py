"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 23/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

from .rlm import RLM
from model_reuse.rlm.available_models import BaselineModels as bM


class BaselineRLM(RLM):
    def __init__(self, model):
        if not isinstance(model, bM):
            raise ValueError('{} is not a valid Baseline Regression Linear Model'.format(model))
        super().__init__(model.value)


class ModelBaseAgo(BaselineRLM):
    def __init__(self):
        super().__init__(bM.BASE_AGO)


class ModelBaseAgoFprofdiff(BaselineRLM):
    def __init__(self):
        super().__init__(bM.BASE_AGO_FPROFDIFF)


class ModelBaseAgoFprofdiffFprof(BaselineRLM):
    def __init__(self):
        super().__init__(bM.BASE_AGO_FPROFDIFF_FPROF)


class ModelBaseAgoSprofdiff(BaselineRLM):
    def __init__(self):
        super().__init__(bM.BASE_AGO_SPROFDIFF)


class ModelBaseAgoSprofdiffSprof(BaselineRLM):
    def __init__(self):
        super().__init__(bM.BASE_AGO_SPROFDIFF_SPROF)


class ModelBaseFprof(BaselineRLM):
    def __init__(self):
        super().__init__(bM.BASE_FPROF)


class ModelBaseSprof(BaselineRLM):
    def __init__(self):
        super().__init__(bM.BASE_SPROF)
