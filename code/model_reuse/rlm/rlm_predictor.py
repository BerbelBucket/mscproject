"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 24/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

import numpy as np

from model_reuse.rlm.available_models import BaselineModels as Bm, StationModels as Sm
from model_reuse.rlm.algorithms import BaselineModelReuseAlgorithms as BAlgorithms
from model_reuse.rlm.algorithms import StationModelReuseAlgorithms as SAlgorithms
from model_reuse.rlm.algorithms import MixedModelReuseAlgorithms as MAlgorithms
from utils.features import Features
from model_reuse.utils.rlm_ops import get_stations, get_docks, get_deployment_bikes, get_deployment_stations
from model_reuse.mae.mae_rlm import MaeRLM
from .baseline_rlm import BaselineRLM as Brlm
from .station_rlm import StationRLM as Srlm

from sklearn.metrics import mean_absolute_error
import scipy.spatial.distance as dist


# from scipy.spatial.distance import euclidean, cosine, jaccard, minkowski, cityblock


class RLMPredictor:
    TEST_DATASET_PATH = './../datasets/testing/'
    EXTENSION = '.csv'
    MODEL_STATION = 'model_station_'
    FIRST_STATION_OLD = 1
    LAST_STATION_OLD = 200
    FIRST_STATION_NEW = 201
    LAST_STATION_NEW = 275
    FIRST_STATION_NEW_TEST = 225
    LAST_WEEK_HOUR = 168

    def __init__(self, test_file: str):
        self.test_data = np.genfromtxt(self.TEST_DATASET_PATH + test_file + self.EXTENSION,
                                       delimiter=',', skip_header=1)
        self.test_data_size = self.test_data.shape[0]
        self.location_of_stations = get_stations()
        self.docks = get_docks()
        self.deployment_stations = get_deployment_stations()
        self.bikes_old = get_deployment_bikes(self.FIRST_STATION_OLD, self.LAST_STATION_OLD)
        self.bikes_new = get_deployment_bikes(self.FIRST_STATION_NEW, self.LAST_STATION_NEW)
        self.mae_dict = MaeRLM()
        self.distances = ['euclidean', 'cosine', 'jaccard', 'cityblock']
        self.week_hour_models = None

    def predict(self, algorithm, model=None, k=0, distance='euclidean', adjust_pred=True):
        if isinstance(algorithm, BAlgorithms):
            if algorithm == BAlgorithms.DEFAULT:
                if not isinstance(model, Bm):
                    raise ValueError("You did not provide a valid model for this algorithm.\n"
                                     "Baseline models: {}".format([m for m in Bm]))
                return self._predict_with_baseline_model(algorithm, model, adjust_pred=adjust_pred)
            if model is not None:
                raise ValueError("You cannot specify a model when using {}".format(algorithm))
            if k <= 0:
                raise ValueError("k must be greater or equal than 1 for the algorithm {}".format(algorithm))

            return self._predict_with_baseline_model(algorithm, k=k, adjust_pred=adjust_pred)

        if isinstance(algorithm, SAlgorithms):
            # Code for K_NEAREST_STATIONS
            if algorithm == SAlgorithms.KN_STATIONS:
                if not isinstance(model, Sm):
                    raise ValueError("You did not provide a valid model for this algorithm.\n"
                                     "Station models: {}".format([m for m in Sm]))
                if k <= 0:
                    raise ValueError("k must be greater or equal than 1 for the algorithm {}".format(algorithm))

                if distance not in self.distances:
                    raise ValueError("You did not provide a valid distance metric.\n"
                                     "Distance metrics: {}".format(self.distances))

                return self._predict_with_station_model(algorithm, model, k, distance, adjust_pred)

            # Code for K_MOST_SIMILAR_DOCKS_STATIONS
            if algorithm == SAlgorithms.K_MOST_SIMILAR_DOCKS_STATIONS:
                if not isinstance(model, Sm):
                    raise ValueError("You did not provide a valid model for this algorithm.\n"
                                     "Station models: {}".format([m for m in Sm]))
                if k <= 0:
                    raise ValueError("k must be greater or equal than 1 for the algorithm {}".format(algorithm))

                if distance not in self.distances:
                    raise ValueError("You did not provide a valid distance metric.\n"
                                     "Distance metrics: {}".format(self.distances))

                return self._predict_with_station_model(algorithm, model, k, distance, adjust_pred)

            # Code for K_MOST_SIMILAR_DOCKS_STATIONS_LOWEST_MAE
            if algorithm == SAlgorithms.K_MOST_SIMILAR_DOCKS_STATIONS_LOWEST_MAE:
                if model is not None:
                    raise ValueError("You cannot specify a model when using {}".format(algorithm))
                if k <= 0:
                    raise ValueError("k must be greater or equal than 1 for the algorithm {}".format(algorithm))

                if distance not in self.distances:
                    raise ValueError("You did not provide a valid distance metric.\n"
                                     "Distance metrics: {}".format(self.distances))

                return self._predict_with_station_model(algorithm, k=k, distance=distance, adjust_pred=adjust_pred)

            # Code for WEIGHTED_K_NEAREST_STATIONS_LOWEST_MAE
            if algorithm == SAlgorithms.WEIGHTED_KN_STATIONS_LOWEST_MAE:
                if model is not None:
                    raise ValueError("You cannot specify a model when using {}".format(algorithm))
                if k <= 0:
                    raise ValueError("k must be greater or equal than 1 for the algorithm {}".format(algorithm))

                if distance not in self.distances:
                    raise ValueError("You did not provide a valid distance metric.\n"
                                     "Distance metrics: {}".format(self.distances))

                return self._predict_with_station_model(algorithm, k=k, distance=distance, adjust_pred=adjust_pred)

            # Code for K_NEAREST_STATIONS_LOWEST_MAE
            if model is not None:
                raise ValueError("You cannot specify a model when using {}".format(algorithm))
            if k <= 0:
                raise ValueError("k must be greater or equal than 1 for the algorithm {}".format(algorithm))

            if distance not in self.distances:
                raise ValueError("You did not provide a valid distance metric.\n"
                                 "Distance metrics: {}".format(self.distances))

            return self._predict_with_station_model(algorithm, k=k, distance=distance, adjust_pred=adjust_pred)

        if isinstance(algorithm, MAlgorithms):
            # Code for WEEK_HOUR_SPECIFIC_LOWEST_MAE
            if algorithm == MAlgorithms.WEEK_HOUR_SPECIFIC_LOWEST_MAE:
                if model is not None:
                    raise ValueError("You cannot specify a model when using {}".format(algorithm))
                if k <= 0:
                    raise ValueError("k must be greater or equal than 1 for the algorithm {}".format(algorithm))

                if distance not in self.distances:
                    raise ValueError("You did not provide a valid distance metric.\n"
                                     "Distance metrics: {}".format(self.distances))

                return self._predict_with_mixed_model(algorithm, k, distance=distance, adjust_pred=adjust_pred)

            # Code for K_NEAREST_STATIONS_LOWEST_MAE
            if model is not None:
                raise ValueError("You cannot specify a model when using {}".format(algorithm))
            if k <= 0:
                raise ValueError("k must be greater or equal than 1 for the algorithm {}".format(algorithm))

            if distance not in self.distances:
                raise ValueError("You did not provide a valid distance metric.\n"
                                 "Distance metrics: {}".format(self.distances))

            return self._predict_with_mixed_model(k, distance=distance, adjust_pred=adjust_pred)

        raise ValueError("You did not provide a valid algorithm.\n"
                         "Baseline algorithms: {}\n"
                         "Station algorithms: {} \n"
                         "Mixed algorithms: {}"
                         .format([a for a in BAlgorithms],
                                 [a for a in SAlgorithms],
                                 [a for a in MAlgorithms]))

    def _predict_with_baseline_model(self, algorithm, model=None, k=0, adjust_pred=True):
        """
        :return: Mean Absolute Error (MAE) of the predictions
        """
        y_true = []
        y_pred = []

        if algorithm == BAlgorithms.DEFAULT:
            predictors = [Brlm(model)]
        else:
            predictors = [Brlm(m) for m in self.mae_dict.get_baseline_model_lowest_mae(k)]

        # We predict the number of bikes for each test instance
        for row in self.test_data:
            prediction = np.mean([p.predict(row) for p in predictors])
            if adjust_pred:
                num_docks = row[Features.NUM_DOCKS.value]
                if prediction > num_docks:
                    prediction = num_docks
                elif prediction < 0:
                    prediction = 0

            true_value = row[Features.BIKES.value]
            y_true.append(true_value)
            y_pred.append(prediction)

        return mean_absolute_error(y_true, y_pred)

    def _predict_with_station_model(self, algorithm, model=None, k=0, distance='euclidean', adjust_pred=True):
        """
        :return: Mean Absolute Error (MAE) of the predictions
        """
        y_true = []
        y_pred = []

        predictors = None
        last_station_id = None
        last_weights = None

        nearest_func = self._nearest_stations_with_distance \
            if algorithm == SAlgorithms.WEIGHTED_KN_STATIONS_LOWEST_MAE \
            else self._nearest_stations

        # We predict the number of bikes for each test instance
        for row in self.test_data:
            station_id = row[Features.STATION.value]
            if station_id != last_station_id:
                docks = row[Features.NUM_DOCKS.value]
                latitude = row[Features.LATITUDE.value]
                longitude = row[Features.LONGITUDE.value]
                nearest_stations = nearest_func((latitude, longitude), k, distance)
                similar_stations_by_docks = self.most_similar_stations_by_docks(docks, k, distance)
                last_station_id = station_id

                # Code for K_NEAREST_STATIONS
                if algorithm == SAlgorithms.KN_STATIONS:
                    predictors = [Srlm(model, s) for s in nearest_stations]

                # Code for K_MOST_SIMILAR_DOCKS_STATIONS
                elif algorithm == SAlgorithms.K_MOST_SIMILAR_DOCKS_STATIONS:
                    predictors = [Srlm(model, s) for s in similar_stations_by_docks]

                # Code for K_NEAREST_STATIONS_LOWEST_MAE
                elif algorithm == SAlgorithms.KN_STATIONS_LOWEST_MAE:
                    models_selected = [self.mae_dict.get_station_model_lowest_mae(s)[0] for s in nearest_stations]
                    predictors = [Srlm(models_selected[i], nearest_stations[i]) for i in range(len(models_selected))]

                # Code for WEIGHTED_K_NEAREST_STATIONS_LOWEST_MAE
                else:
                    ns_id = [s[0] for s in nearest_stations]
                    models_selected = [self.mae_dict.get_station_model_lowest_mae(s)[0] for s in ns_id]
                    predictors = [Srlm(models_selected[i], ns_id[i]) for i in range(len(models_selected))]

                    ns_dist = [s[1] for s in nearest_stations]
                    last_weights = [1 / d for d in ns_dist]
                    weights_sum = sum(last_weights)
                    last_weights = [w / weights_sum for w in last_weights]
                    # print(weights_sum, last_weights)

                    if station_id == 226:
                        print(ns_id, last_weights)

            if algorithm == SAlgorithms.WEIGHTED_KN_STATIONS_LOWEST_MAE:
                prediction = np.sum([predictors[i].predict(row) * last_weights[i] for i in range(len(predictors))])

            else:
                prediction = np.mean([p.predict(row) for p in predictors])

            if adjust_pred:
                num_docks = row[Features.NUM_DOCKS.value]
                if prediction > num_docks:
                    prediction = num_docks

                elif prediction < 0:
                    prediction = 0

            true_value = row[Features.BIKES.value]
            y_true.append(true_value)
            y_pred.append(prediction)

        return mean_absolute_error(y_true, y_pred)

    def _predict_with_mixed_model(self, algorithm, k=0, distance='euclidean', adjust_pred=True):
        """
        :return Mean Absolute Error (MAE) of the predictions
        """

        if algorithm == MAlgorithms.WEEK_HOUR_SPECIFIC_LOWEST_MAE:
            return self.predict_with_best_week_hour_model(k, distance)

        y_true = []
        y_pred = []

        predictors = None
        last_station_id = None

        # Code for K_NEAREST_STATIONS_LOWEST_MAE
        # We predict the number of bikes for each test instance
        for row in self.test_data:
            station_id = row[Features.STATION.value]
            if station_id != last_station_id:
                latitude, longitude = row[Features.LATITUDE.value], row[Features.LONGITUDE.value]
                nearest_stations = self._nearest_stations((latitude, longitude), k, distance)
                last_station_id = station_id

                models_selected = [self.mae_dict.get_model_lowest_mae(s)[0] for s in nearest_stations]
                predictors = [Srlm(models_selected[i], nearest_stations[i]) if isinstance(models_selected[i], Sm)
                              else Brlm(models_selected[i])
                              for i in range(len(models_selected))]

            prediction = np.mean([p.predict(row) for p in predictors])

            if adjust_pred:
                num_docks = row[Features.NUM_DOCKS.value]
                if prediction > num_docks:
                    prediction = num_docks

                elif prediction < 0:
                    prediction = 0

            y_true.append(row[Features.BIKES.value])
            y_pred.append(prediction)

        return mean_absolute_error(y_true, y_pred)

    def predict_with_best_station_model_deployment(self, model=None, n=3, adjust_pred=True):
        """
        :return: Mean Absolute Error (MAE) of the predictions
        """
        y_true = []
        y_pred = []

        last_station_id = None
        predictors = None

        # We predict the number of bikes for each test instance
        for row in self.test_data:
            station_id = row[Features.STATION.value]
            if station_id != last_station_id:
                predictors = self._best_station_predictors_deployment(station_id, model, n)
                last_station_id = station_id

            prediction = np.mean([p.predict(row) for p in predictors])

            if adjust_pred:
                num_docks = row[Features.NUM_DOCKS.value]
                if prediction > num_docks:
                    prediction = num_docks

                elif prediction < 0:
                    prediction = 0

            y_true.append(row[Features.BIKES.value])
            y_pred.append(prediction)

        return mean_absolute_error(y_true, y_pred)

    def _nearest_stations(self, location, n: int = 1, distance='euclidean'):
        metric = getattr(dist, distance)
        distances = [(k, metric(location, v)) for k, v in self.location_of_stations.items()]
        return list(map(lambda x: x[0], sorted(distances, key=lambda x: x[1])[:n]))

    def _nearest_stations_with_distance(self, location, n: int = 1, distance='euclidean'):
        metric = getattr(dist, distance)
        distances = [(k, metric(location, v)) for k, v in self.location_of_stations.items()]
        return list(sorted(distances, key=lambda x: x[1])[:n])

    def _best_station_predictors_deployment(self, station_id, model=None, n=3):
        maes = [(Srlm(model, s), self._get_mae_deployment(Srlm(model, s),
                                                          self.deployment_stations[station_id]))
                for s in range(self.FIRST_STATION_OLD, self.LAST_STATION_OLD + 1)]

        return list(map(lambda x: x[0], sorted(maes, key=lambda x: x[1])[:n]))

    @staticmethod
    def _get_mae_deployment(predictor, deployment_station):
        y_true = []
        y_pred = []

        for row in deployment_station:
            y_true.append(row[Features.BIKES.value])
            y_pred.append(predictor.predict(row))

        return mean_absolute_error(y_true, y_pred)

    def most_similar_stations_by_docks(self, docks, n=1, distance='euclidean'):
        metric = getattr(dist, distance)
        distances = [(k, metric(docks, v)) for k, v in self.docks.items()]
        return list(map(lambda x: x[0], sorted(distances, key=lambda x: x[1])[:n]))

    def _predict_with_baseline_model_week_hour(self, algorithm, week_hour, model=None, k=0, adjust_pred=True):
        """
        :return: Mean Absolute Error (MAE) of the predictions
        """
        y_true = []
        y_pred = []

        if algorithm == BAlgorithms.DEFAULT:
            predictors = [Brlm(model)]
        else:
            predictors = [Brlm(m) for m in self.mae_dict.get_baseline_model_lowest_mae(k)]

        for partial_data in self.deployment_stations.values():
            for row in partial_data[partial_data[:, Features.WEEK_HOUR.value] == week_hour]:
                prediction = np.mean([p.predict(row) for p in predictors])
                if adjust_pred:
                    num_docks = row[Features.NUM_DOCKS.value]
                    if prediction > num_docks:
                        prediction = num_docks
                    elif prediction < 0:
                        prediction = 0

                true_value = row[Features.BIKES.value]
                y_true.append(true_value)
                y_pred.append(prediction)

        return mean_absolute_error(y_true, y_pred)

    def _predict_with_station_model_week_hour(self, algorithm, week_hour, model=None,
                                              k=0, distance='euclidean', adjust_pred=True):
        """
        :return: Mean Absolute Error (MAE) of the predictions
        """
        y_true = []
        y_pred = []
        predictors = None
        last_station_id = None

        for partial_data in self.deployment_stations.values():
            for row in partial_data[partial_data[:, Features.WEEK_HOUR.value] == week_hour]:
                station_id = row[Features.STATION.value]
                if station_id != last_station_id:
                    latitude, longitude = row[Features.LATITUDE.value], row[Features.LONGITUDE.value]
                    nearest_stations = self._nearest_stations((latitude, longitude), k, distance)
                    last_station_id = station_id

                    if algorithm == SAlgorithms.KN_STATIONS:
                        predictors = [Srlm(model, s) for s in nearest_stations]
                    else:
                        models_selected = [self.mae_dict.get_station_model_lowest_mae(s)[0] for s in nearest_stations]
                        predictors = [Srlm(models_selected[i], nearest_stations[i])
                                      for i in range(len(models_selected))]

                prediction = np.mean([p.predict(row) for p in predictors])

                if adjust_pred:
                    num_docks = row[Features.NUM_DOCKS.value]
                    if prediction > num_docks:
                        prediction = num_docks

                    elif prediction < 0:
                        prediction = 0

                true_value = row[Features.BIKES.value]
                y_true.append(true_value)
                y_pred.append(prediction)

        return mean_absolute_error(y_true, y_pred)

    def predict_with_best_week_hour_model(self, k=3, distance='euclidean'):
        """
        :return: Mean Absolute Error (MAE) of the predictions
        """

        # Firstly, we need to compute the best week hour models if they are not computed yet
        if self.week_hour_models is None:
            self._compute_best_week_hour_models(k, distance)

        y_true = []
        y_pred = []
        last_station_id = None
        last_nearest_stations = None

        # We predict the number of bikes for each test instance
        for row in self.test_data:
            week_hour = int(row[Features.WEEK_HOUR.value])
            best_model = self.week_hour_models[week_hour - 1]
            if isinstance(best_model, Bm):
                predictors = [Brlm(best_model)]
            else:
                station_id = row[Features.STATION.value]
                if station_id != last_station_id:
                    latitude, longitude = row[Features.LATITUDE.value], row[Features.LONGITUDE.value]
                    last_nearest_stations = self._nearest_stations((latitude, longitude), k, distance)
                    last_station_id = station_id

                predictors = [Srlm(best_model, s) for s in last_nearest_stations]

            prediction = np.mean([p.predict(row) for p in predictors])

            num_docks = row[Features.NUM_DOCKS.value]
            if prediction > num_docks:
                prediction = num_docks
            elif prediction < 0:
                prediction = 0

            true_value = row[Features.BIKES.value]
            y_true.append(true_value)
            y_pred.append(prediction)

        return mean_absolute_error(y_true, y_pred)

    def _compute_best_week_hour_models(self, k=3, distance='euclidean'):
        """
        Compute the best week hour models (the ones with lower MAE for each week hour
        and store them in a class variable.

        :param k: number of neighbors to consider
        """
        best_week_hour_models = []
        for w in range(1, self.LAST_WEEK_HOUR + 1):
            print("Processing Week hour {}...".format(w))
            best_current_model = None
            best_current_mae = float('inf')

            # We find the baseline model with the lower MAE for week hour w
            b_algorithm = BAlgorithms.DEFAULT
            for model in Bm:
                result = self._predict_with_baseline_model_week_hour(b_algorithm, w, model)
                if best_current_mae > result:
                    best_current_mae = result
                    best_current_model = model

            # We find the station-specific model with the lower MAE for week hour w
            s_algorithm = SAlgorithms.KN_STATIONS
            for model in Sm:
                result = self._predict_with_station_model_week_hour(s_algorithm, w, model, k, distance)
                if best_current_mae > result:
                    best_current_mae = result
                    best_current_model = model

            # We append the model with lower MAE to the list
            best_week_hour_models.append(best_current_model)

        print("Best week hour models have been computed and stored in the variable week_hour_models")
        self.week_hour_models = best_week_hour_models
