"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 26/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

from enum import Enum


class BaselineModelReuseAlgorithms(Enum):
    DEFAULT = 'Baseline Default'
    LOWEST_MAE = 'Baseline Lowest MAE'


class StationModelReuseAlgorithms(Enum):
    KN_STATIONS = 'K-Nearest Stations'
    KN_STATIONS_LOWEST_MAE = 'K-Nearest Stations Lowest MAE'
    WEIGHTED_KN_STATIONS_LOWEST_MAE = 'Weighted K-Nearest Stations Lowest MAE'
    K_MOST_SIMILAR_DOCKS_STATIONS = 'K-Most Similar Docks Stations'
    K_MOST_SIMILAR_DOCKS_STATIONS_LOWEST_MAE = 'K-Most Similar Docks Stations Lowest MAE'


class MixedModelReuseAlgorithms(Enum):
    K_NEAREST_STATIONS_LOWEST_MAE = 'K-Nearest Stations Lowest MAE'
    WEEK_HOUR_SPECIFIC_LOWEST_MAE = 'Week hour Specific Lowest MAE'
    # WEEK_HOUR_AND_STATION_SPECIFIC_LOWEST_MAE = 'Week hour and Station Specific Lowest MAE'
