"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 26/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""


class Station:
    def __init__(self, station_id, latitude, longitude):
        self.station_id = station_id
        self.latitude = latitude
        self.longitude = longitude
