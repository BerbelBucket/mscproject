"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 23/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""
import csv

from model_reuse.rlm.available_models import BaselineModels as Bm, StationModels as Sm
from utils.features import Features

import numpy as np

MODELS_PATH = './../models'
DATASETS_PATH = './../datasets'
MAE_PATH = './../mae'
MAE_BY_STATION = f'{MAE_PATH}/models_mae_on_deploy_by_station.csv'
MAE_SUMMARY = f'{MAE_PATH}/models_mae_on_deploy_summary.csv'
DEPLOYMENT_DATASETS = f'{DATASETS_PATH}/deployment'


def get_features_from_csv(file: str):
    """
    Reads a file and returns a dictionary with the corresponding features and weights
    :param file: regression linear model
    :return: dictionary with features and weights
    """

    file_path = f'{MODELS_PATH}/{file}'
    intercept = float('inf')
    features_dict = {}

    with open(file_path) as f:
        reader = csv.DictReader(f, delimiter=',')
        for row in reader:
            feature = row['feature']
            weight = float(row['weight'])
            if feature == '(Intercept)':
                intercept = weight
            else:
                features_dict[feature] = weight

    return intercept, features_dict


def get_mae_by_station_from_csv():
    """
    Reads a csv file and returns a dictionary with the corresponding models and MAE
    :return: dictionary with the MAE of the models by station
    """

    mae_dict = {}

    with open(MAE_BY_STATION) as f:
        reader = csv.DictReader(f, delimiter=',')
        for row in reader:
            station = int(row['station'])
            try:
                model = Sm(row['model'])
            except ValueError:
                model = Bm(row['model'])
            mae = float(row['mae'])

            if station not in mae_dict:
                mae_dict[station] = {}
            mae_dict[station][model] = mae

    return mae_dict


def get_mae_summary_from_csv():
    """
    Reads a csv file and returns a dictionary with the corresponding models and MAE
    :return: dictionary with the MAE of the models
    """

    mae_dict = {}

    with open(MAE_SUMMARY) as f:
        reader = csv.DictReader(f, delimiter=',')
        for row in reader:
            try:
                model = Sm(row['model'])
            except ValueError:
                model = Bm(row['model'])
            mae = float(row['mae'])
            mae_dict[model] = mae

    return mae_dict


def get_station(station_id: int):
    file = f'{DEPLOYMENT_DATASETS}/station_{station_id}_deploy'
    if station_id > 200:
        file += '.csv'

    else:
        file += '_full.csv'
    with open(file) as f:
        reader = csv.reader(f, delimiter=',')
        next(reader)
        row = next(reader)
        return float(row[Features.LATITUDE.value]), float(row[Features.LONGITUDE.value])


def get_stations(from_station: int = 1, to_station: int = 200):
    stations_dict = {}
    for i in range(from_station, to_station + 1):
        stations_dict[i] = get_station(i)

    return stations_dict


def _get_docks(station_id: int):
    file = f'{DEPLOYMENT_DATASETS}/station_{station_id}_deploy_full.csv'
    with open(file) as f:
        reader = csv.reader(f, delimiter=',')
        next(reader)
        row = next(reader)
        return int(row[Features.NUM_DOCKS.value])


def get_docks(from_station: int = 1, to_station: int = 200):
    stations_dict = {}
    for i in range(from_station, to_station + 1):
        stations_dict[i] = _get_docks(i)

    return stations_dict


def get_bikes(station_id: int):
    file = f'{DEPLOYMENT_DATASETS}/station_{station_id}_deploy'
    if station_id > 200:
        file += '.csv'

    else:
        file += '_full.csv'

    dataset = np.genfromtxt(file, delimiter=',', skip_header=1)[:, Features.BIKES.value]
    return dataset[~np.isnan(dataset)]


def get_deployment_bikes(from_station: int = 1, to_station: int = 200):
    bikes_dict = {}
    for i in range(from_station, to_station + 1):
        bikes_dict[i] = get_bikes(i)

    return bikes_dict


def get_deployment_station(station_id):
    file = f'{DEPLOYMENT_DATASETS}/station_{station_id}_deploy'
    if station_id > 200:
        file += '.csv'

    else:
        file += '_full.csv'

    dataset = np.genfromtxt(file, delimiter=',', skip_header=1)
    rows_to_remove = np.isnan(dataset).sum(axis=1) > 1
    return dataset[~rows_to_remove]


def get_deployment_stations(from_station=226, to_station=275):
    stations_dict = {}
    for i in range(from_station, to_station + 1):
        stations_dict[i] = get_deployment_station(i)

    return stations_dict
