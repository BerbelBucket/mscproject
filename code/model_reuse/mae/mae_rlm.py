"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 23/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

from model_reuse.utils.rlm_ops import get_mae_by_station_from_csv, get_mae_summary_from_csv
from model_reuse.rlm.available_models import BaselineModels as Bm
from model_reuse.rlm.available_models import StationModels as Sm


class MaeRLM:
    """
    The MaeRLM object contains the MAE for all the regression linear models.
    """

    def __init__(self):
        self.mae_by_station = get_mae_by_station_from_csv()
        self.mae_summary = get_mae_summary_from_csv()

    def get_mae_summary_of(self, model: str):
        """
        :param model: name of the regression linear model
        :return: MAE of the model
        """
        return self.mae_summary[model]

    def get_mae_of_station(self, station: int, model: str):
        """
        :param station: id of the station
        :param model: name of the regression linear model
        :return: MAE of the model
        """
        return self.mae_by_station[station][model]

    def get_baseline_model_lowest_mae(self, n=1):
        """
        :param n: number of baseline models to return
        :return: n lowest MAE models
        """
        b_models = list(filter(lambda x: isinstance(x[0], Bm), self.mae_summary.items()))
        return [x[0] for x in sorted(b_models, key=lambda x: x[1])[:n]]

    def get_station_model_lowest_mae(self, station_id, n=1):
        """
        :param station_id: id of the station
        :param n: number of station-oriented models to return
        :return: n lowest MAE models
        """
        s_models = list(filter(lambda x: isinstance(x[0], Sm), self.mae_by_station[station_id].items()))
        return [x[0] for x in sorted(s_models, key=lambda x: x[1])[:n]]

    def get_model_lowest_mae(self, station_id, n=1):
        """
        :param station_id: id of the station
        :param n: number of models to return
        :return: n lowest MAE models
        """
        return [x[0] for x in sorted(self.mae_by_station[station_id].items(), key=lambda x: x[1])[:n]]
