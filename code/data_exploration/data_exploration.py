"""
Author: PabloBerbel (pablo.marin@mycit.ie)
"""

import pandas as pd
import matplotlib.pyplot as plt
from pandas import DataFrame
from pandas.api.types import is_numeric_dtype
from model_reuse.utils.rlm_ops import get_stations
from model_reuse.rlm.rlm_predictor import RLMPredictor
from scipy.spatial.distance import euclidean


def show_first_n_elements(df: DataFrame, n: int = 10):
    print(df.head(n))


def get_irrelevant_cols(df: DataFrame):
    cols = []
    for col in df.columns:
        if is_numeric_dtype(df[col]) and df[col].min() == df[col].max():
            cols.append(col)

    return cols


def calculate_statistics(df: DataFrame, irr_columns):
    for col in df.columns:
        if is_numeric_dtype(df[col]) and col not in irr_columns:
            print("{}: ".format(col))
            print("\t Mean = {}".format(df[col].mean()))
            print("\t Standard deviation = {}".format(df[col].std()))
            print("\t Minimum = {}".format(df[col].min()))
            print("\t Maximum = {}".format(df[col].max()))


def get_correlation(df: DataFrame, cols):
    return df[cols].corr()


def get_covariance(df: DataFrame, cols):
    return df[cols].cov()


def display_summary(df: DataFrame):
    print(df.describe(include='all'))


def display_histogram(df: DataFrame, col: str, bins: int = 8):
    df.hist(column=col, bins=bins)
    plt.show()
    # plt.savefig("histogram.pdf")


def show_boxplot(df: DataFrame):
    df.boxplot(["bikes", "bikes_3h_ago"])
    plt.show()


def show_scatter(df: DataFrame):
    cols = [("bikes", "weekhour"),
            ("bikes", "isHoliday"),
            ("bikes", "full_profile_bikes"),
            ("bikes", "short_profile_bikes")]
    n_rows = 2
    n_cols = 2
    size = 12
    fig, axes = plt.subplots(n_rows, n_cols, figsize=(size, size))

    index = 0
    for i in range(n_rows):
        for j in range(i + 1, n_rows + 1):
            ax1 = int(index / n_cols)
            ax2 = index % n_cols

            col1 = cols[index][0]
            col2 = cols[index][1]
            axes[ax1][ax2].scatter(df[col1], df[col2], color='red')
            axes[ax1][ax2].set_xlabel(col1)
            axes[ax1][ax2].set_ylabel(col2)
            index = index + 1

    plt.show()


def plot_locations():
    locations = get_stations(to_station=275)
    latitudes = list(map(lambda x: x[0], locations.values()))
    longitudes = list(map(lambda x: x[1], locations.values()))
    plt.scatter(latitudes[:200], longitudes[:200], c='b', label='Old stations')

    # for i in range(275):
    #     plt.text(latitudes[i], longitudes[i], i + 1, fontsize=12)

    plt.title('Location of the stations')
    plt.xlabel('Latitude')
    plt.ylabel('Longitude')
    plt.legend()

    plt.show()


def plot_kns_example(station=226):
    locations = get_stations(to_station=275)
    old_stations = [i for i in range(1, 201)]
    station_location = locations[station]
    distances = [(i, euclidean(station_location, locations[i])) for i in old_stations]
    ordered_stations = list(map(lambda x: x[0], sorted(distances, key=lambda x: x[1])[:3]))

    loc = [locations[x] for x in ordered_stations]
    latitudes = list(map(lambda x: x[0], loc))
    longitudes = list(map(lambda x: x[1], loc))
    plt.scatter(latitudes, longitudes, c='b', label='Old K-Nearest stations')

    plt.scatter(locations[station][0], locations[station][1], c='r', label='New station')

    for i in range(len(ordered_stations)):
        plt.text(latitudes[i] + 0.0002, longitudes[i] + 0.0002, ordered_stations[i], fontsize=14, color='b')
    plt.text(station_location[0] + 0.0002, station_location[1] + 0.0002, station, fontsize=14, color='r')

    plt.title('K-Nearest Stations')
    plt.xlabel('Latitude')
    plt.ylabel('Longitude')
    plt.legend()

    plt.show()


if __name__ == '__main__':
    file_new = "./../../datasets/deployment/station_1_deploy_full.csv"
    file_old = "./../../datasets/deployment/station_226_deploy.csv"
    # new_station_df = pd.read_csv(file_new)
    # old_station_df = pd.read_csv(file_old)
    # pd.set_option('display.max_columns', None)
    # show_first_n_elements(station_data)
    # irrelevant_columns = get_irrelevant_cols(new_station_df)
    # calculate_statistics(station_df, irrelevant_columns)
    # display_summary(station_df)
    # display_histogram(station_df, "bikes")
    # show_boxplot(station_df)
    # show_scatter(station_df)

    # my_cols = ["bikes", "hour", "bikes_3h_ago", "weekhour", "isHoliday", "full_profile_bikes", "short_profile_bikes"]
    # new_station_df = new_station_df[my_cols]
    # old_station_df = old_station_df[my_cols]

    # plt.scatter(new_station_df["bikes"], new_station_df["weekhour"])
    # plt.plot(new_station_df["bikes"], new_station_df["hour"])
    # plt.show()

    # Plot locations
    # plot_locations()

    # KNS example
    plot_kns_example()
