"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 22/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""
from model_reuse.rlm.rlm_predictor import RLMPredictor
import model_reuse.rlm.available_models as models
from model_reuse.rlm.algorithms import BaselineModelReuseAlgorithms as BAlgorithms
from model_reuse.rlm.algorithms import StationModelReuseAlgorithms as SAlgorithms
from model_reuse.rlm.algorithms import MixedModelReuseAlgorithms as MAlgorithms
from utils.features import Features

from model_training.decision_tree import DecisionTree
from model_training.decision_tree_new import DecisionTreeNew

from model_training.knn_new import KnnNew

import matplotlib.pyplot as plt
import numpy as np
import time


def run_baseline(adjust_pred=True):
    algorithm = BAlgorithms.DEFAULT
    for model in models.BaselineModels:
        start_time = time.time()
        result = rlm_predictor.predict(algorithm, model, adjust_pred=adjust_pred)
        finish_time = round(time.time() - start_time, 2)

        print("Algorithm: {}; Model: {}; Adjust predictions: {}; "
              "Time: {}; MAE: {}"
              .format(algorithm, model, adjust_pred, finish_time, result))


def run_baseline_lowest_mae(k=3, adjust_pred=True):
    algorithm = BAlgorithms.LOWEST_MAE
    plt.xlabel('K')
    plt.ylabel('MAE')
    x = [i for i in range(k + 1)]
    xi = [i for i in range(len(x))]
    plt.xticks(xi, x)

    res_k = []
    res_mae = []
    for i in range(1, k + 1):
        start_time = time.time()
        result = rlm_predictor.predict(algorithm, k=i, adjust_pred=adjust_pred)
        finish_time = round(time.time() - start_time, 2)
        res_k.append(i)
        res_mae.append(result)
        print("Algorithm: {}; k: {}; Adjust predictions: {}; Time: {}; "
              "MAE: {}"
              .format(algorithm, i, adjust_pred, finish_time, result))

    min_mae = min(res_mae)
    max_mae = max(res_mae)
    plt.plot(res_k, res_mae, marker='', color='r', linewidth=2)
    plt.ylim(min_mae - 0.05, max_mae + 0.05)
    adj = 'adjusting predictions' if adjust_pred else 'without adjusting predictions'
    plt.title("Algorithm: {} {}".format(algorithm.value, adj))
    plt.show()


def run_baseline_lowest_mae_both(k=3):
    algorithm = BAlgorithms.LOWEST_MAE
    colors = ['red', 'blue']
    plt.xlabel('K')
    plt.ylabel('MAE')
    x = [i for i in range(k + 1)]
    xi = [i for i in range(len(x))]
    plt.xticks(xi, x)

    min_mae = float('inf')
    max_mae = float('-inf')
    index = 0
    for b in [False, True]:
        res_k = []
        res_mae = []
        for i in range(1, k + 1):
            start_time = time.time()
            result = rlm_predictor.predict(algorithm, k=i, adjust_pred=b)
            finish_time = round(time.time() - start_time, 2)
            res_k.append(i)
            res_mae.append(result)
            print("Algorithm: {}; k: {}; Adjust predictions: {}; Time: {}; "
                  "MAE: {}"
                  .format(algorithm, i, b, finish_time, result))

        label = 'Adjusting predictions' if b else 'Without adjusting predictions'
        plt.plot(res_k, res_mae, label=label, marker='', color=colors[index], linewidth=2)
        index += 1

        current_min_mae = min(res_mae)
        current_max_mae = max(res_mae)

        if current_min_mae < min_mae:
            min_mae = current_min_mae

        if current_max_mae > max_mae:
            max_mae = current_max_mae

    plt.ylim(min_mae - 0.05, max_mae + 0.15)
    plt.legend(loc='upper right')
    plt.title("Algorithm: {}".format(algorithm.value))
    plt.show()


def run_k_nearest_stations(k=3, distance='euclidean', adjust_pred=True):
    algorithm = SAlgorithms.KN_STATIONS
    colors = ['red', 'blue', 'green', 'orange', 'black', 'purple']
    plt.xlabel('K')
    plt.ylabel('MAE')
    x = [i for i in range(k + 1)]
    xi = [i for i in range(len(x))]
    plt.xticks(xi, x)

    min_mae = float('inf')
    max_mae = float('-inf')
    index = 0
    for model in models.StationModels:
        res_k = []
        res_mae = []
        for i in range(1, k + 1):
            start_time = time.time()
            result = rlm_predictor.predict(algorithm, model, i, distance, adjust_pred)
            finish_time = round(time.time() - start_time, 2)
            res_k.append(i)
            res_mae.append(result)
            print("Algorithm: {}; k: {}; Model: {}; Adjust predictions: {}; Time: {}; "
                  "MAE: {}"
                  .format(algorithm, i, model, adjust_pred, finish_time, result))

        plt.plot(res_k, res_mae, label=model.value, marker='', color=colors[index], linewidth=2)
        index += 1

        current_min_mae = min(res_mae)
        current_max_mae = max(res_mae)

        if current_min_mae < min_mae:
            min_mae = current_min_mae

        if current_max_mae > max_mae:
            max_mae = current_max_mae

    plt.ylim(min_mae - 0.05, max_mae + 0.15)
    adj = 'adjusting predictions' if adjust_pred else 'without adjusting predictions'
    plt.title("Algorithm: {} with {} distance \n and {}".format(algorithm.value, distance, adj))
    plt.legend(loc='upper right')
    plt.show()


def run_k_nearest_stations_lowest_mae(k=3, adjust_pred=True):
    algorithm = SAlgorithms.KN_STATIONS_LOWEST_MAE
    colors = ['red', 'blue', 'green', 'black']
    plt.xlabel('K')
    plt.ylabel('MAE')
    x = [i for i in range(k + 1)]
    xi = [i for i in range(len(x))]
    plt.xticks(xi, x)

    min_mae = float('inf')
    max_mae = float('-inf')
    index = 0
    for distance in rlm_predictor.distances:
        res_k = []
        res_mae = []
        for i in range(1, k + 1):
            start_time = time.time()
            result = rlm_predictor.predict(algorithm, k=i, distance=distance, adjust_pred=adjust_pred)
            finish_time = round(time.time() - start_time, 2)
            res_k.append(i)
            res_mae.append(result)
            print("Algorithm: {}; k: {}; Distance: {}; Adjust predictions: {}; Time: {}; "
                  "MAE: {}"
                  .format(algorithm, i, distance, adjust_pred, finish_time, result))

        plt.plot(res_k, res_mae, label=distance, marker='', color=colors[index], linewidth=2)
        index += 1

        current_min_mae = min(res_mae)
        current_max_mae = max(res_mae)

        if current_min_mae < min_mae:
            min_mae = current_min_mae

        if current_max_mae > max_mae:
            max_mae = current_max_mae

    plt.ylim(min_mae - 0.05, max_mae + 0.05)
    adj = 'adjusting predictions' if adjust_pred else 'without adjusting predictions'
    plt.title("Algorithm: {} {}".format(algorithm.value, adj))
    plt.legend(loc='best')
    plt.show()


def run_weighted_k_nearest_stations_lowest_mae(k=3, adjust_pred=True):
    algorithm = SAlgorithms.WEIGHTED_KN_STATIONS_LOWEST_MAE
    colors = ['red', 'blue', 'green', 'black']
    plt.xlabel('K')
    plt.ylabel('MAE')
    x = [i for i in range(k + 1)]
    xi = [i for i in range(len(x))]
    plt.xticks(xi, x)

    min_mae = float('inf')
    max_mae = float('-inf')
    index = 0
    for distance in rlm_predictor.distances:
        res_k = []
        res_mae = []
        for i in range(1, k + 1):
            start_time = time.time()
            result = rlm_predictor.predict(algorithm, k=i, distance=distance, adjust_pred=adjust_pred)
            finish_time = round(time.time() - start_time, 2)
            res_k.append(i)
            res_mae.append(result)
            print("Algorithm: {}; k: {}; Distance: {}; Adjust predictions: {}; Time: {}; "
                  "MAE: {}"
                  .format(algorithm, i, distance, adjust_pred, finish_time, result))

        plt.plot(res_k, res_mae, label=distance, marker='', color=colors[index], linewidth=2)
        index += 1

        current_min_mae = min(res_mae)
        current_max_mae = max(res_mae)

        if current_min_mae < min_mae:
            min_mae = current_min_mae

        if current_max_mae > max_mae:
            max_mae = current_max_mae

    plt.ylim(min_mae - 0.05, max_mae + 0.05)
    adj = 'adjusting predictions' if adjust_pred else 'without adjusting predictions'
    plt.title("Algorithm: {} {}".format(algorithm.value, adj))
    plt.legend(loc='best')
    plt.show()


def run_k_best_station_models_deployment(k=3, adjust_pred=True):
    colors = ['red', 'blue', 'green', 'orange', 'black', 'purple']
    plt.xlabel('K')
    plt.ylabel('MAE')
    x = [i for i in range(k + 1)]
    xi = [i for i in range(len(x))]
    plt.xticks(xi, x)

    min_mae = float('inf')
    max_mae = float('-inf')
    index = 0
    for model in models.StationModels:
        res_k = []
        res_mae = []
        for i in range(1, k + 1):
            start_time = time.time()
            result = rlm_predictor.predict_with_best_station_model_deployment(model, i, adjust_pred)
            finish_time = round(time.time() - start_time, 2)
            res_k.append(i)
            res_mae.append(result)
            print(
                "Algorithm: Best station models based on deployment data; k: {}; "
                "Model: {}; Adjust predictions: {}; Time: {}; MAE: {}".format(i,
                                                                              model,
                                                                              adjust_pred,
                                                                              finish_time,
                                                                              result))

        plt.plot(res_k, res_mae, label=model.value, marker='', color=colors[index], linewidth=2)
        index += 1

        current_min_mae = min(res_mae)
        current_max_mae = max(res_mae)

        if current_min_mae < min_mae:
            min_mae = current_min_mae

        if current_max_mae > max_mae:
            max_mae = current_max_mae

    plt.ylim(min_mae - 0.05, max_mae + 0.15)
    adj = 'adjusting predictions' if adjust_pred else 'without adjusting predictions'
    plt.title("Algorithm: K-Best stations models based on deployment data \n {}".format(adj))
    plt.legend(loc='upper right')
    plt.show()


def run_best_weekhour(k=3):
    algorithm = MAlgorithms.WEEK_HOUR_SPECIFIC_LOWEST_MAE
    colors = ['red', 'blue', 'green', 'black']
    plt.xlabel('K')
    plt.ylabel('MAE')
    x = [i for i in range(k + 1)]
    xi = [i for i in range(len(x))]
    plt.xticks(xi, x)

    min_mae = float('inf')
    max_mae = float('-inf')
    index = 0
    for distance in rlm_predictor.distances:
        res_k = []
        res_mae = []
        for i in range(1, k + 1):
            start_time = time.time()
            result = rlm_predictor.predict(algorithm, k=i, distance=distance)
            finish_time = round(time.time() - start_time, 2)
            res_k.append(i)
            res_mae.append(result)
            print("Algorithm: {}; k: {}; Distance: {}; Adjust predictions: {}; Time: {}; "
                  "MAE: {}"
                  .format(algorithm, i, distance, True, finish_time, result))

        plt.plot(res_k, res_mae, label=distance, marker='', color=colors[index], linewidth=2)
        index += 1

        current_min_mae = min(res_mae)
        current_max_mae = max(res_mae)

        if current_min_mae < min_mae:
            min_mae = current_min_mae

        if current_max_mae > max_mae:
            max_mae = current_max_mae

    plt.ylim(min_mae - 0.05, max_mae + 0.05)
    plt.title("Algorithm: {} adjusting predictions".format(algorithm.value))
    plt.legend(loc='best')
    plt.show()


if __name__ == '__main__':
    test_dataset = 'morebikes_own_full_test_data'
    rlm_predictor = RLMPredictor(test_dataset)
    k_neighbors = 10

    # Baseline without adjusting predictions
    # adjust_predictions = False
    # run_baseline(adjust_predictions)

    # Weekhour adjusting predictions
    # run_best_weekhour(10)

    # Baseline Lowest MAE without adjusting predictions
    # adjust_predictions = False
    # run_baseline_lowest_mae(k=7, adjust_pred=adjust_predictions)

    # Baseline Lowest MAE adjusting predictions
    # adjust_predictions = True
    # run_baseline_lowest_mae(k=7, adjust_pred=adjust_predictions)

    # Baseline Lowest MAE adjusting and without adjusting predictions
    # run_baseline_lowest_mae_both(k=7)

    # K-Nearest Stations without adjusting predictions
    # adjust_predictions = False
    # run_k_nearest_stations(k_neighbors, adjust_pred=adjust_predictions)

    # K-Nearest Stations adjusting predictions
    # adjust_predictions = True
    # run_k_nearest_stations(k_neighbors, adjust_pred=adjust_predictions)

    # K-Nearest Stations Lowest MAE Models without adjusting predictions
    # adjust_predictions = False
    # run_k_nearest_stations_lowest_mae(k_neighbors, adjust_predictions)

    # K-Nearest Stations Lowest MAE Models adjusting predictions
    # adjust_predictions = True
    # run_k_nearest_stations_lowest_mae(k_neighbors, adjust_predictions)

    # Weighted K-Nearest Stations Lowest MAE Models adjusting predictions
    # adjust_predictions = True
    # run_weighted_k_nearest_stations_lowest_mae(k_neighbors, adjust_predictions)

    dt = DecisionTree()
    dt.set_features([Features.BIKES_3H_AGO,
                     Features.SHORT_PROFILE_BIKES,
                     Features.SHORT_PROF_3H_DIFF_BIKES,
                     Features.FULL_PROF_BIKES,
                     Features.FULL_PROF_3H_DIFF_BIKES])
    dt.normalize()
    dt.fit()
    print("Decision Tree R2", dt.r2())
    print("Decision Tree MAE", dt.predict())

    dt2 = DecisionTreeNew()
    dt2.set_features([Features.BIKES_3H_AGO,
                     Features.SHORT_PROFILE_BIKES,
                     Features.SHORT_PROF_3H_DIFF_BIKES,
                     Features.FULL_PROF_BIKES,
                     Features.FULL_PROF_3H_DIFF_BIKES])
    dt2.normalize()
    dt2.fit()
    print("Decision Tree New R2", dt2.r2())
    print("Decision Tree New MAE", np.mean(dt2.predict()))

    knn = KnnNew()
    knn.set_features([Features.BIKES_3H_AGO,
                      Features.SHORT_PROFILE_BIKES,
                      Features.SHORT_PROF_3H_DIFF_BIKES,
                      Features.FULL_PROF_BIKES,
                      Features.FULL_PROF_3H_DIFF_BIKES])
    knn.normalize()
    knn.fit()
    print("KNN MAE:", np.mean(knn.predict()))

    # K-Best station models based on deployment data adjusting predictions
    # adjust_predictions = False
    # run_k_nearest_stations_lowest_mae(k_neighbors, adjust_predictions)

    # K-Best station models based on deployment data adjusting predictions
    # adjust_predictions = True
    # run_k_best_station_models_deployment(k_neighbors, adjust_predictions)

    # print("K_MOST_SIMILAR_DOCKS_STATIONS_LOWEST_MAE")
    # for i in range(1, 6):
    #     print(rlm_predictor.predict(SAlgorithms.K_MOST_SIMILAR_DOCKS_STATIONS_LOWEST_MAE, k=i))
    #
    # print("K_MOST_SIMILAR_DOCKS_STATIONS")
    # for model in models.StationModels:
    #     for i in range(1, 6):
    #         print(rlm_predictor.predict(SAlgorithms.K_MOST_SIMILAR_DOCKS_STATIONS, model, i))
