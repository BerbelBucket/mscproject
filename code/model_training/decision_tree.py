"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 16/04/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

from .model_training import ModelTraining
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_absolute_error

import numpy as np


class DecisionTree(ModelTraining):
    def __init__(self):
        super().__init__()
        self.regressor = None
        self.trained_model = None

    def fit(self):
        self.regressor = DecisionTreeRegressor(random_state=0)
        self.trained_model = self.regressor.fit(self.X_train, self.y_train)

    def r2(self):
        return self.regressor.score(self.X_test, self.y_test)

    def predict(self):
        y_predicted = self.trained_model.predict(self.X_test)
        return mean_absolute_error(self.y_test, y_predicted)
