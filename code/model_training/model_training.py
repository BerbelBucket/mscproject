"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 16/04/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

import numpy as np
from sklearn import preprocessing
from utils.features import Features
from abc import ABC, abstractmethod

TRAIN_PATH = './../datasets/deployment/'
TEST_PATH = './../datasets/testing/'
TEST_FILE = 'morebikes_own_full_test_data.csv'

FIRST_STATION = 1
LAST_STATION = 200

FIRST_FEATURE_INDEX = 0
LAST_FEATURE_INDEX = 23


def _train_file(station_id: int):
    return TRAIN_PATH + f'station_{station_id}_deploy_full.csv'


def _test_file():
    return TEST_PATH + TEST_FILE


def _get_train_files():
    station_ids = list(range(FIRST_STATION, LAST_STATION + 1))
    train_data = [np.genfromtxt(_train_file(i), delimiter=',', skip_header=1) for i in station_ids]

    return np.concatenate(train_data)


def _get_test_file():
    return np.genfromtxt(_test_file(), delimiter=',', skip_header=1)


class ModelTraining(ABC):
    def __init__(self):
        self.train_dataset = _get_train_files()
        self.X_train = []
        self.y_train = self.train_dataset[:, Features.BIKES.value]

        self.test_dataset = _get_test_file()
        self.X_test = []
        self.y_test = self.test_dataset[:, Features.BIKES.value]

    def set_features(self, features):
        if all(isinstance(f, Features) for f in features):
            self.X_train = self.train_dataset[:, [f.value for f in features]]
            self.X_test = self.test_dataset[:, [f.value for f in features]]

        elif all(isinstance(f, int) and FIRST_FEATURE_INDEX <= f <= LAST_FEATURE_INDEX for f in features):
            self.X_train = self.train_dataset[:, features]
            self.X_test = self.test_dataset[:, features]

        elif all(isinstance(f, str) for f in features):
            self.X_train = self.train_dataset[:, [Features.get_index(f) for f in features]]
            self.X_test = self.test_dataset[:, [Features.get_index(f) for f in features]]

        else:
            raise ValueError("You did not provide a valid option for features")

        rows_to_remove = np.logical_or(np.isnan(self.X_train).any(axis=1), np.isnan(self.y_train))
        self.X_train = self.X_train[~rows_to_remove]
        self.y_train = self.y_train[~rows_to_remove]

    def normalize(self):
        self.X_train = preprocessing.normalize(self.X_train)
        self.X_test = preprocessing.normalize(self.X_test)

    @abstractmethod
    def predict(self):
        pass
