"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 22/04/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

import numpy as np
from sklearn import preprocessing
from utils.features import Features
from abc import ABC, abstractmethod

TRAIN_PATH = './../datasets/deployment/'
TEST_PATH = './../datasets/testing/'
TEST_FILE = 'morebikes_own_full_test_data.csv'

FIRST_STATION = 226
LAST_STATION = 275

FIRST_FEATURE_INDEX = 0
LAST_FEATURE_INDEX = 23


def _train_file(station_id: int):
    return TRAIN_PATH + f'station_{station_id}_deploy.csv'


def _test_file():
    return TEST_PATH + TEST_FILE


def _get_train_files():
    station_ids = list(range(FIRST_STATION, LAST_STATION + 1))
    train_data = [np.genfromtxt(_train_file(i), delimiter=',', skip_header=1) for i in station_ids]

    return train_data
    # return np.concatenate(train_data)


def _get_test_files():
    full_test_file = np.genfromtxt(_test_file(), delimiter=',', skip_header=1)
    return [full_test_file[full_test_file[:, Features.STATION.value] == i]
            for i in range(FIRST_STATION, LAST_STATION + 1)]


class ModelTrainingNew(ABC):
    def __init__(self):
        self.train_datasets = _get_train_files()
        self.X_train = []
        self.y_train = [t[:, Features.BIKES.value] for t in self.train_datasets]

        self.test_datasets = _get_test_files()
        self.X_test = []
        self.y_test = [t[:, Features.BIKES.value] for t in self.test_datasets]

        self.n = len(self.train_datasets)

    def set_features(self, features):
        if all(isinstance(f, Features) for f in features):
            self.X_train = [t[:, [f.value for f in features]] for t in self.train_datasets]
            self.X_test = [t[:, [f.value for f in features]] for t in self.test_datasets]

        elif all(isinstance(f, int) and FIRST_FEATURE_INDEX <= f <= LAST_FEATURE_INDEX for f in features):
            self.X_train = [t[:, features] for t in self.train_datasets]
            self.X_test = [t[:, features] for t in self.test_datasets]

        elif all(isinstance(f, str) for f in features):
            self.X_train = [t[:, [Features.get_index(f) for f in features]] for t in self.train_datasets]
            self.X_test = [t[:, [Features.get_index(f) for f in features]] for t in self.test_datasets]

        else:
            raise ValueError("You did not provide a valid option for features")

        rows_to_remove = [np.logical_or(np.isnan(self.X_train[i]).any(axis=1), np.isnan(self.y_train[i]))
                          for i in range(self.n)]
        self.X_train = [self.X_train[i][~rows_to_remove[i]] for i in range(self.n)]
        self.y_train = [self.y_train[i][~rows_to_remove[i]] for i in range(self.n)]

    def normalize(self):
        self.X_train = [preprocessing.normalize(x) for x in self.X_train]
        self.X_test = [preprocessing.normalize(x) for x in self.X_test]

    @abstractmethod
    def predict(self):
        pass
