"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 23/04/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

from .model_training_new import ModelTrainingNew
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import mean_absolute_error
from utils.features import Features


class KnnNew(ModelTrainingNew):
    def __init__(self):
        super().__init__()
        self.regressors = None
        self.trained_models = None

    def fit(self):
        self.regressors = [KNeighborsRegressor(3) for _ in range(self.n)]
        self.trained_models = [self.regressors[i].fit(self.X_train[i], self.y_train[i])
                               for i in range(self.n)]

    def r2(self):
        return [self.regressors[i].score(self.X_test[i],
                                         self.y_test[i])
                for i in range(self.n)]

    def predict(self):
        y_predicted = [self.trained_models[i].predict(self.X_test[i])
                       for i in range(self.n)]

        return [mean_absolute_error(self.y_test[i], y_predicted[i])
                for i in range(self.n)]
