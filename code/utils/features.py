"""
Author: PabloBerbel (pablo.marin@mycit.ie)
Date: 23/03/2019
Project for the MSc in Artificial Intelligence at Cork Institute of Technology
"""

from enum import Enum

features = ["station", "latitude", "longitude", "numDocks", "timestamp",
            "year", "month", "day", "hour", "weekday", "weekhour", "isHoliday",
            "windMaxSpeed.m.s", "windMeanSpeed.m.s", "windDirection.grades",
            "temperature.C", "relHumidity.HR", "airPressure.mb", "precipitation.l.m2",
            "bikes_3h_ago", "full_profile_3h_diff_bikes", "full_profile_bikes",
            "short_profile_3h_diff_bikes", "short_profile_bikes", "bikes"]


class Features(Enum):
    STATION = 0
    LATITUDE = 1
    LONGITUDE = 2
    NUM_DOCKS = 3
    TIMESTAMP = 4
    YEAR = 5
    MONTH = 6
    DAY = 7
    HOUR = 8
    WEEK_DAY = 9
    WEEK_HOUR = 10
    IS_HOLIDAY = 11
    WIND_MAX_SPEED = 12
    WIND_MIN_SPEED = 13
    WIND_DIRECTION = 14
    TEMPERATURE = 15
    REL_HUMIDITY = 16
    AIR_PRESSURE = 17
    PRECIPITATION = 18
    BIKES_3H_AGO = 19
    FULL_PROF_3H_DIFF_BIKES = 20
    FULL_PROF_BIKES = 21
    SHORT_PROF_3H_DIFF_BIKES = 22
    SHORT_PROFILE_BIKES = 23
    BIKES = 24

    @staticmethod
    def get_index(feature: str):
        return features.index(feature)
